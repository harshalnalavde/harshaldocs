it includes summary charts showing the evolution over time of the following indicators:

Number of executions, mean execution times and percentage of errors of http requests, sql requests, jsf actions, struts actions, jsp pages or methods of business fa�ades (if EJB3, Spring or Guice)
Java memory
Java CPU
Number of user sessions
Number of jdbc connections
These charts can be viewed on the current day, week, month, year or custom period.

JavaMelody includes statistics of predefined counters (currently http requests, sql requests, jsf actions, struts actions, jsp pages and methods of business fa�ades if EJB3, Spring or Guice) with, for each counter :

A summary indicating the overall number of executions, the average execution time, the cpu time and the percentage of errors.
And the percentage of time spent in the requests for which the average time exceeds a configurable threshold.
And the complete list of requests, aggregated without dynamic parameters with, for each, the number of executions, the mean execution time, the mean cpu time, the percentage of errors and an evolution chart of execution time over time.
Furthermore, each http request indicates the size of the flow response, the mean number of sql executions and the mean sql time.